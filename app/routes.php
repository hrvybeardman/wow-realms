<?php
Flight::route('/', function() {
    Flight::render('index');
});

Flight::route('/realm-list.json',function() {
//    error_reporting(0);

    $cache = new Cache;
    $list = $cache->getOrQuery('realm-list', 600, function() {
        $url = 'http://eu.battle.net/api/wow/realm/status';
        $list = file_get_contents($url);

        if ( $list === false ) {
            $list = json_encode(['status'=>'error']);
        }

        return $list;
    });

    Flight::json($list);
});