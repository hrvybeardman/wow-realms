<?php

class Cache extends Blablacar\Memcached\Client {
    public function __construct()
    {
        parent::__construct();

        $this->addServer('127.0.0.1',11211);
    }

    public function getOrQuery($key, $time=300, $callback)
    {
        $exists = $this->get($key);
        if ( $this->getResultCode() == Memcached::RES_NOTFOUND ) {
            $result = json_decode(call_user_func($callback),true);
            $result['fetch_time'] = time();
            $this->set($key,$result,$time);
            return $result;
        } else {
            return $exists;
        }
    }
} 