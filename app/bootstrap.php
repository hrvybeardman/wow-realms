<?php

require '../vendor/autoload.php';

if ( preg_match('/^dev\./',$_SERVER['SERVER_NAME']) ) {
    define(DEBUG,true);
    error_reporting(E_ALL);
    ini_set('display_errors',1);

    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}

Flight::set('flight.views.path','../app/views');
Flight::set('flight.handle_errors',false);

require 'routes.php';

Flight::start();