<!doctype html>
<html lang="en" ng-app="WowRealmStatus">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
    <title>World of Warcraft Realm Status</title>

    <link href="/assets/vendor/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/app/css/app.css" rel="stylesheet">

    <script src="/assets/vendor/angular/angular.min.js"></script>
</head>
<body ng-controller="RealmListController">
<header>
    <h1>World of Warcraft Realm Status</h1>
    <div class="container-fluid">
        <div class="row">
            <div class="query col-xs-12 col-sm-3 col-md-6 col-lg-6">
                <div class="form-group">
                    <input type="text" ng-model="query.name" class="form-control" placeholder="Filter by server name">
                </div>
            </div>
<!--            <div class="regions col-xs-12">-->
<!--                <label>Europe <input class="region eu" value="eu" type="checkbox"></label>-->
<!--                <label>US <input class="region us" value="us" type="checkbox"></label>-->
<!--                <label>Korea <input class="region kr" value="kr" type="checkbox"></label>-->
<!--                <label>China <input class="region cn" value="cn" type="checkbox"></label>-->
<!--                <label>Taiwan <input class="region tw" value="tw" type="checkbox"></label>-->
<!--            </div>-->
        </div>
    </div>
</header>

<div class="container-fluid realms">
    <div class="row">
        <div class="realm col-xs-12 col-sm-6 col-md-3 col-lg-2" ng-repeat="realm in realms | filter:query">
            <div class="status {{realm.population}} {{realm.status && !realm.queue ? 'up' : 'down' }}">
                <div class="icon">
                    <i class="glyphicon glyphicon-ok" ng-if="realm.status == true && realm.queue == false"></i>
                    <i class="glyphicon glyphicon-lock" ng-if="realm.status == true && realm.queue == true"></i>
                    <i class="glyphicon glyphicon-remove" ng-if="realm.status == false"></i>
                </div>
                <div class="name">{{realm.name}}</div>
                <div class="meta">
                    <span class="type">{{realm.type | realmType}}</span> | <span class="language">{{realm.locale | readable}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="/assets/app/js/app.js"></script>
</body>
</html>