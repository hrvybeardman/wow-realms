var app = angular.module('WowRealmStatus',[]);

app.controller('RealmListController',function($scope, $http) {
    $http.get('realm-list.json').success(function(data) {
        if ( null == data.status ) {
            console.log(data.fetch_time);
            $scope.last_fetch = data.fetch_time;
            $scope.realms = data.realms;
        }
    });
});

app.filter('realmType',function() {
    return function(input) {
        var out = 'PvE';
        var typeMap = {
            "pvp": "PvP",
            "pve": "PvE",
            "rp": "RP",
            "rppvp": "RP PvP"
        };

        if ( null != typeMap[input] ) {
            out = typeMap[input];
        }

        return out;
    }
});

app.filter('readable',function() {
    return function(input) {
        var out = 'English';
        var localeMap = {
            'en_US': "English",
            'en_GB': "English",
            'es_MX': "Spanish",
            'es_ES': "Spanish",
            'pt_BR': "Portuguese (BR)",
            'pt_PT': "Portuguese",
            'fr_FR': "French",
            'ru_RU': "Russian",
            'de_DE': "German",
            'it_IT': "Italian",
            'ko_KR': "Korean",
            'zh_TW': "Taiwanese",
            'zh_CH': "Chinese"
        };

        if ( null != localeMap[input] ) {
            out = localeMap[input];
        }

        return out;
    }
})